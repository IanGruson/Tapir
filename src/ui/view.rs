use std::collections::HashMap;
use tui::{
	text::{Span, Line, Text},
	widgets::ListItem,
};

use crate::{database::container, Application::{VariableKeyMap, VarKey}};

pub fn container_to_spans<T>(container: Vec<T>) -> Vec<Line<'static>>
where
	T: container::Container,
{
	let mut spans_vec = vec![];
	for item in container.iter() {
		spans_vec.push(Line::from(Span::raw(item.name())));
	}

	spans_vec
}

pub fn container_to_ListItem<T>(container: Vec<T>) -> Vec<ListItem<'static>>
where
	T: container::Container,
{
	let mut list_items = vec![];
	for item in container.iter() {
		list_items.push(ListItem::new(item.name()));
	}

	list_items
}

pub fn element_to_ListItem<T>(element: Vec<T>) -> Vec<ListItem<'static>>
where
	T: container::Element,
{
	let mut list_items = vec![];
	for item in element.iter() {
		list_items.push(ListItem::new(item.name()));
	}

	list_items
}

pub fn variable_to_hash_map<Variable>(
	element: Vec<container::Variable>,
) -> VariableKeyMap<(i64, String)> {
	let mut variable_map = VariableKeyMap::new();

	let mut idx: usize = 0;
	for item in element.iter() {
		let var_key = VarKey {
			variable_key: item.key(),
			list_index: idx, 
			
		};
		variable_map.insert(var_key, (item.id, item.value()));
		idx += 1;
	}
	variable_map

}
