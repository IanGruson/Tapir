use tui::{
	text::Line,
	widgets::{ListItem, ListState},
};

use crate::Application;

pub enum Movement {
	UP,
	RIGHT,
	DOWN,
	LEFT,
}

// Move inside the list a certain amount of steps (eg: 2 steps up)
pub fn select(list: &Vec<ListItem>, list_state: &mut ListState, movement: Movement, amount: usize) {
	let i = list_state.selected().unwrap();
	match movement {
		Movement::UP => {
			if i == 0 {
				list_state.select(Some(list.len() - amount));
			} else {
				list_state.select(Some(i - amount));
			}
		}
		Movement::DOWN => {
			if i >= list.len() - amount {
				list_state.select(Some(0));
			} else {
				list_state.select(Some(i + amount));
			}
		}
		_ => {}
	}
}

pub fn tab_select(tabs: &Vec<Line>, tab_state: &mut ListState, movement: Movement) {
	match movement {
		Movement::LEFT => {
			if tab_state.selected().unwrap() == 0 {
				tab_state.select(Some(tabs.len() - 1));
			} else {
				tab_state.select(Some(tab_state.selected().unwrap() - 1));
			}
		}
		Movement::RIGHT => {
			if tab_state.selected().unwrap() < tabs.len() - 1 {
				tab_state.select(Some(tab_state.selected().unwrap() + 1));
			} else {
				tab_state.select(Some(0));
			}
		}
		_ => {}
	}
}

pub fn selection_mode_navigate(app: &mut Application::App, movement: Movement, amount: usize) {
	match app.selection_mode {
		Application::SelectionMode::Requests => {
			select(&app.request_list, &mut app.req_state, movement, amount);
		}
		Application::SelectionMode::Collections => {
			app.req_state.select(Some(0));
			select(&app.collection_list, &mut app.col_state, movement, amount);
		}

		Application::SelectionMode::Response => match movement {
			Movement::UP => {
				if app.response_scroll > 0 {
					app.response_scroll -= 1;
				}
			}
			Movement::DOWN => {
				app.response_scroll += 1;
			}
			_ => {}
		},
	}
}
