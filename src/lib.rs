pub mod database;
pub mod sender;
pub mod ui;
pub mod util;

pub mod Application {
	use termion::event::Key;
	use tui::{
		backend::Backend,
		layout::{Constraint, Direction, Layout, Rect},
		style::{Color, Style},
		terminal::Frame,
		text::{Span, Line},
		widgets::{Block, Borders, Clear, ListItem, ListState, Paragraph},
	};

	use super::database::container::*;
	use std::{collections::HashMap, rc::Rc, fmt::Debug};

	const SPACES: &str = "    ";

	#[derive(Debug, PartialEq, Eq)]
	pub enum InputMode {
		Normal,
		Command,
		Editing,
		HelpControls,
		HelpCommands,
	}

	pub enum SelectionMode {
		Collections,
		Requests,
		Response,
	}

	#[derive(Debug, PartialEq, Eq)]
	pub enum VarEditingMode {
		None,
		Key,
		Value,
		Validating,
	}

	pub struct App<'a> {
		pub input: String,
		pub input_command: String,
		pub input_mode: InputMode,
		pub var_editing_mode: VarEditingMode,
		pub selection_mode: SelectionMode,
		pub selected_tab: usize, // Stores the selected tabs
		pub workspaces: Vec<Workspace>,
		pub workspaces_line: Vec<Line<'a>>,
		pub tab_len: usize,             // Stores the number of tabs
		pub workspace_state: ListState, // Stores state for workspaces list
		pub col_state: ListState,       // Stores state for collections list
		pub req_state: ListState,       // Stores state for requests list
		pub collections: Vec<Collection>,
		pub requests: Vec<Request>,
		pub collection_list: Vec<ListItem<'a>>, // Holds the actual collection list
		pub request_list: Vec<ListItem<'a>>,    // Holds the actual request list
		pub variables: Vec<Variable>,
		pub variables_map: VariableKeyMap<(i64, String)>,
		pub variables_list: Vec<ListItem<'a>>,
		pub variable_state: ListState,
		pub response: Vec<String>, // Stores the reponse data.
		pub response_scroll: u16,
		pub controls_line: Vec<Line<'a>>,
		pub event_input: Key,
		pub environment_kv_input: (String, String),
	}

	impl<'a> Default for App<'a> {
		fn default() -> App<'a> {
			App {
				input: String::new(),
				input_command: String::new(),
				input_mode: InputMode::Normal,
				var_editing_mode: VarEditingMode::None,
				selection_mode: SelectionMode::Collections,
				selected_tab: 0,
				workspaces: Vec::new(),
				workspaces_line: Vec::new(),
				tab_len: 0,
				workspace_state: ListState::default(),
				col_state: ListState::default(),
				req_state: ListState::default(),
				collections: Vec::new(),
				requests: Vec::new(),
				collection_list: Vec::new(),
				request_list: Vec::new(),
				variables: Vec::new(),
				variables_map: VariableKeyMap::new(),
				variables_list: Vec::new(),
				variable_state: ListState::default(),
				response: Vec::new(),
				response_scroll: 0,
				controls_line: Vec::new(),
				event_input: Key::Null,
				environment_kv_input: (String::from(""), String::from("")),
			}
		}
	}

	#[derive(Clone, Debug, Eq, PartialEq, Hash)]
	pub struct VarKey {
		pub variable_key: String,
		pub list_index: usize,
	}

	#[derive(Clone, Debug, Eq, Hash, PartialEq)]
	pub enum KeyQuery<'s> {
		VarKey(&'s str),
		Index(usize),
	}

	#[derive(Debug)]
	pub struct VariableKeyMap<V>
	{
		pub variables_map: HashMap<String, Rc<V>>,
		pub variables_index: HashMap<usize, Rc<V>>,
	}

	impl<V> VariableKeyMap<V> 
	where
		V: Debug + PartialEq,
	{
		pub fn new() -> Self {
			Self {
				variables_map: HashMap::new(),
				variables_index: HashMap::new(),
			}
		}

		pub fn insert(&mut self, key: VarKey, value: V) -> Option<V> {
			let value = Rc::new(value);
			let existing = self.variables_map.insert(key.variable_key, value.clone());
			assert_eq!(
				existing,
				self.variables_index.insert(key.list_index, value.clone())
			);
			existing.map(|e| Rc::into_inner(e).unwrap())
		}

		pub fn get(&self, kq: &KeyQuery) -> Option<&V> {
			match kq {
				KeyQuery::VarKey(s) => self.variables_map.get(*s).map(Rc::as_ref),
				KeyQuery::Index(i) => self.variables_index.get(i).map(Rc::as_ref),
			}
		}
	}

	pub fn centered_rect(x_percent: u16, y_percent: u16, rect: Rect) -> Rect {
		let layout = Layout::default()
			.direction(Direction::Vertical)
			.constraints(
				[
					Constraint::Percentage((100 - y_percent) / 2),
					Constraint::Percentage(y_percent),
					Constraint::Percentage((100 - y_percent) / 2),
				]
				.as_ref(),
			)
			.split(rect);
		Layout::default()
			.direction(Direction::Horizontal)
			.constraints(
				[
					Constraint::Percentage((100 - x_percent) / 2),
					Constraint::Percentage(x_percent),
					Constraint::Percentage((100 - x_percent) / 2),
				]
				.as_ref(),
			)
			.split(layout[1])[1]
	}

	pub fn render_control_footer<B: Backend>(
		f: &mut Frame<B>,
		chunks: &Rc<[Rect]>,
		controls_map: HashMap<String, String>,
		position: usize,
		app: &mut App,
	) {
		let contrast_style = Style::default().bg(Color::White).fg(Color::Black);

		let mut span_vec = Vec::new();
		if app.controls_line.is_empty() {
			for (key, desc) in controls_map {
				span_vec.push(Span::styled(key, contrast_style));
				span_vec.push(Span::raw(": "));
				span_vec.push(Span::raw(desc));
				span_vec.push(Span::raw(SPACES));
			}
			app.controls_line = vec![Line::from(span_vec)];
		}
		let control_bar = Paragraph::new(app.controls_line.clone()).block(Block::default());
		f.render_widget(control_bar, chunks[position]);
	}

	pub fn render_help_box<B: Backend>(
		f: &mut Frame<B>,
		help_strings: Vec<Line>,
		x_percent: u16,
		y_percent: u16,
	) {
		let help_box = Paragraph::new(help_strings).block(
			Block::default()
				.title("HELP COMMAND PROMPT")
				.borders(Borders::ALL),
		);
		let area = centered_rect(x_percent, y_percent, f.size());
		f.render_widget(Clear, area);
		f.render_widget(help_box, area);
	}
}
