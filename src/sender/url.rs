use crate::database::container::Request;

use curl::easy::{Easy2, Handler, WriteError};
use std::*;

struct Collector(Vec<u8>);

impl Handler for Collector {
	fn write(&mut self, data: &[u8]) -> Result<usize, WriteError> {
		self.0.extend_from_slice(data);
		Ok(data.len())
	}
}

pub fn send_request(request: Request) -> Result<Vec<String>, io::Error> {
	let mut easy = Easy2::new(Collector(Vec::new()));
	easy.get(true)?;
	easy.url(&request.url)?;
	easy.perform()?;

	// assert_eq!(easy.response_code().unwrap(), 200);
	let contents = easy.get_ref();
	Ok(vec![String::from_utf8(contents.0.clone()).unwrap()])
}
