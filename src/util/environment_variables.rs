use std::error::Error;

use tui::{backend::Backend, layout::Rect, widgets::ListItem, Frame};

use crate::database::container::{Variable, self};
use crate::Application;
use unicode_width::UnicodeWidthStr;

use lazy_static::lazy_static;
use regex::Regex;

use super::dbhandler::Database;

pub fn cursor_for_env<B: Backend>(
	f: &mut Frame<B>,
	chunk: Rect,
	list_item: ListItem,
	list_index: u16,
	app: &mut Application::App,
) {
	let selected_item_y_pos =
		chunk.y + list_item.height() as u16 + list_item.height() as u16 * list_index;
	f.set_cursor(
		chunk.x as u16 + app.input.width() as u16 + 1,
		selected_item_y_pos as u16,
	)
}

pub fn get_key_from_list_item(string: &String) -> String {
	lazy_static! {
		static ref KEY: Regex = Regex::new(r"\w+").unwrap();
	}

	let s = match KEY.find(string) {
		Some(mat) => {
			let cap = KEY.captures(mat.as_str()).unwrap();
			cap[0].to_string()
		}
		None => String::from("")
	};

	s
}

pub fn save_variable(db: &Database, var: Variable) -> Result<(), Box<dyn Error>> {

	container::update_variable(var.id, var.key.as_str(), var.value.as_str(), db)?;

	Ok(())
}
