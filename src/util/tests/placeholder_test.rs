#[cfg(test)]
mod tests {

use crate::{util::placeholder, Application::{VariableKeyMap, VarKey}};

	#[test]
	fn should_replace_placeholder_in_url() {
		let mut variables_map = VariableKeyMap::new();
		let var_key = VarKey {variable_key: String::from("username"), list_index: 1};
		variables_map.insert(var_key, (1 ,String::from("John")));

		let mut url: String = String::from("http://test-url.xyz/profile/{{username}}/");
		assert_eq!(
			String::from("http://test-url.xyz/profile/John/"),
			placeholder::replace_placeholder_in_string(&variables_map, &mut url)
		);
	}

	#[test]
	fn should_replace_multiple_placeholders_in_url() {
		let mut variables_map = VariableKeyMap::new();
		let username_var_key = VarKey {variable_key: String::from("username"), list_index: 1};
		let env_var_key = VarKey {variable_key: String::from("env"), list_index: 2};
		variables_map.insert(username_var_key, (1 ,String::from("John")));
		variables_map.insert(env_var_key, (2 ,String::from("prod")));

		let mut url: String = String::from("http://test-url-{{env}}.xyz/profile/{{username}}/");

		let base = String::from("http://test-url-prod.xyz/profile/John/");
		let res = placeholder::replace_placeholder_in_string(&variables_map, &mut url);
		assert_eq!(base, res, "testing between {} and {}", base, res);
	}

	#[test]
	fn should_not_replace_placeholder_in_url() {
		let mut variables_map = VariableKeyMap::new();
		let var_key = VarKey {variable_key: String::from("username"), list_index: 1};
		variables_map.insert(var_key, (1 ,String::from("John")));

		let mut url: String = String::from("http://test-url.xyz/profile/{{ username }}/");
		assert_ne!(
			String::from("http://test-url.xyz/profile/John/"),
			placeholder::replace_placeholder_in_string(&variables_map, &mut url)
		);
	}
}
