#[cfg(test)]
mod tests {

use crate::util::environment_variables::get_key_from_list_item;

	#[test]
	fn should_replace_placeholder_in_url() {
		let res = get_key_from_list_item(&String::from("key1 = value1"));
		dbg!(&res);
		assert_eq!(
			res,
			String::from("key1")
		);
	}
}
