use lazy_static::lazy_static;
use regex::Regex;
use std::collections::hash_map::{DefaultHasher, HashMap};

use crate::Application::{VariableKeyMap, KeyQuery};

pub fn replace_placeholder_in_string(
	variables_map: &VariableKeyMap<(i64, String)>,
	string: &mut String,
) -> String {
	lazy_static! {
		static ref PLACEHOLDER: Regex = Regex::new(r"\{{2}(\w*)}{2}").unwrap();
	}

	let temp_string: &mut String = string;
	let s = match PLACEHOLDER.find(temp_string) {
		Some(mat) => {
			let cap = PLACEHOLDER.captures(mat.as_str()).unwrap();
			let key = &cap[1];
			let key_query = KeyQuery::VarKey(key);
			let value = variables_map.get(&key_query).unwrap();
			temp_string.replace_range(mat.range(), &value.1);
			replace_placeholder_in_string(&variables_map, temp_string)
		}
		None => temp_string.clone(),
	};

	s
}
