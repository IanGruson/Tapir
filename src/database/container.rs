use crate::util::dbhandler::*;
use std::str::FromStr;

use std::fmt;

use super::user::*;
use sqlite::{*, Error};

macro_rules! ok(($result:expr) => ($result.unwrap()));

#[derive(Debug, PartialEq, Clone)]
pub enum Methods {
	GET,
	POST,
	DELETE,
	MODIFY,
	PUT,
}

impl FromStr for Methods {
	type Err = ();
	fn from_str(input: &str) -> std::result::Result<Methods, Self::Err> {
		match input {
			"GET" => Ok(Methods::GET),
			"POST" => Ok(Methods::POST),
			"DELETE" => Ok(Methods::DELETE),
			"MODIFY" => Ok(Methods::MODIFY),
			"PUT" => Ok(Methods::PUT),
			_ => Err(()),
		}
	}
}

// Convert enum to String
	impl fmt::Display for Methods {
		fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
			write!(f, "{:?}", self)
		}
	}

	#[derive(Clone)]
	pub struct Workspace {
		pub id: i64,
		pub name: String,
		pub collections: Vec<Collection>,
	}

	#[derive(Clone)]
	pub struct Collection {
		pub id: i64,
		pub name: String,
		pub queries: Vec<Request>,
	}

	#[derive(Clone)]
	pub struct Environmnent {
		pub id: i64,
		pub name: String,
	}

	pub trait Container {
		fn new(id: i64, name: String) -> Self;
		fn name(&self) -> String;
	}

	impl Container for Workspace {
		fn new(id: i64, name: String) -> Workspace {
			Workspace {
				id: id,
				name: name,
				collections: vec![],
			}
		}

		fn name(&self) -> String {
			self.name.clone()
		}
	}

	impl Container for Collection {
		fn new(id: i64, name: String) -> Self {
			Self {
				id: id,
				name: name,
				queries: vec![],
			}
		}

		fn name(&self) -> String {
			self.name.clone()
		}
	}

	pub trait Element {
		fn name(&self) -> String;
	}

	#[derive(Clone)]
	pub struct Request {
		pub id: i64,
		pub name: String,
		pub method: Methods,
		pub url: String,
		pub params: String,
		pub body: String,
	}

	#[derive(Clone, Debug)]
	pub struct Variable {
		pub id: i64,
		pub key: String,
		pub value: String,
	}

	impl Variable {
		pub fn key(&self) -> String {
			self.key.clone()
		}

		pub fn value(&self) -> String {
			self.value.clone()
		}
	}

	impl Element for Request {
		fn name(&self) -> String {
			self.name.clone()
		}
	}

	impl Element for Variable {
		fn name(&self) -> String {
			self.key.clone()
		}
	}

	/// Creates a Workspace in the database.
	///
	/// params :
	/// name - the name of the workspace that will be created in the database.
	/// db - a database object.
	pub fn create_workspace(user: &User, name: &str, db: &Database) -> Result<()> {
		//This methods works and is not vulnerable to SQL injections because of the vec! I think.
		//Also doesn't work without a statement and I don't get why.
		let mut statement = db
			.connection
			.prepare("INSERT INTO Workspace(name) VALUES (:name);")?;
		statement.bind_iter([(":name", Value::String(name.into()))])?;
		statement.next()?;

		// Need to get the id of the workspace that's just been created.
		//
		statement = db
			.connection
			.prepare("SELECT rowid FROM Workspace WHERE name = :name;")
			.unwrap();
		statement.bind_iter(vec![(":name", Value::String(name.to_owned()))])?;
		while let Ok(State::Row) = statement.next() {
				//get the id of the last created Workspace.
				let workspace_id = statement.read::<i64, _>("id").unwrap();

				// Join table insert statement
				statement = db.connection.prepare("INSERT INTO User_Workspace(id_user, id_workspace) VALUES (:id_user, :id_workspace);")?;
				statement.bind_iter(vec![
					(":id_user", Value::Integer(user.id.into())),
					(":id_workspace", Value::Integer(workspace_id.into())),
				])?;
			// None => Err(Error {
			// 	code: Some(1),
			// 	message: Some(String::from(
			// 		"Something went wrong, User_Workspace entry not created.",
			// 	)),
			// }),
		}
		Ok(())
	}

	/// Creates a Collection in the database.
	///
	/// params :
	/// name - the name of the collection that will be created in the database.
	/// workspace_id : the workspace id this collection will be attached to.
	/// db - a database object.
	pub fn create_collection(name: &str, workspace_id: i64, db: &Database) -> Result<()> {
		let mut statement = db
			.connection
			.prepare("INSERT INTO Collection(name, id_workspace) VALUES (:name, :id_workspace);")?;
		statement.bind_iter(vec![
			(":name", Value::String(name.to_owned())),
			(":id_workspace", Value::Integer(workspace_id.into())),
		])?;
		statement.next()?;
		Ok(())
	}

	/// Creates a new HTTP Request for a Collection.
	///
	/// params :
	///
	/// name - the name of the request.
	/// id_collection - the collection the request will be attached to.
	/// method - the HTTP method (GET, POST ...).
	/// url - the url of the request.
	/// db - a database object.
	pub fn create_request(
		name: &str,
		id_collection: i64,
		method: &str,
		url: &str,
		db: &Database,
	) -> Result<()> {
		let mut statement = db.connection.prepare("INSERT INTO Request(name, id_collection, method, url) VALUES (:name, :id_collection, :method, :url);")?;
		statement.bind_iter(vec![
			(":name", Value::String(name.to_owned())),
			(":id_collection", Value::Integer(id_collection.into())),
			(":method", Value::String(method.into())),
			(":url", Value::String(url.into())),
		])?;
		statement.next()?;
		Ok(())
	}

	/// Delete a workspace from it's name.
	///
	/// * `id` - i64 integer of the corresponding workspace to delete.
	/// * `db` - Database to work on.
	pub fn delete_workspace(id: i64, db: &Database) -> Result<()> {
		db.connection.prepare("PRAGMA foreign_keys = on")?;

		let mut statement = db
			.connection
			.prepare("DELETE FROM Workspace WHERE id = :id;")?;
		statement.bind_iter(vec![(":id", Value::Integer(id.to_owned()))])?;
		statement.next()?;
		Ok(())
	}

	/// Delete a collection from it's id.
	///
	/// * `id` - i64 integer of the corresponding collection to delete.
	/// * `db` - Database to work on.
	pub fn delete_collection(id: i64, db: &Database) -> Result<()> {
		db.connection.prepare("PRAGMA foreign_keys = on")?;
		let mut statement = db
			.connection
			.prepare("DELETE FROM Collection WHERE id = :id;")?;
		statement.bind_iter(vec![(":id", Value::Integer(id.to_owned()))])?;
		statement.next()?;
		Ok(())
	}

	/// Delete a collection from it's name.
	///
	/// * `id` - i64 integer of the corresponding request to delete.
	/// * `db` - Database to work on.
	pub fn delete_request(id: i64, db: &Database) -> Result<()> {
		let mut statement = db
			.connection
			.prepare("DELETE FROM Request WHERE id = :id;")?;
		statement.bind_iter(vec![(":id", Value::Integer(id.to_owned()))])?;
		statement.next()?;
		Ok(())
	}

	/// Fetches all workspaces from a user's id.
	///
	/// Returns a Vec of Workspaces Struct wrapped in sqlite's Result.
	pub fn get_all_workspaces(id_user: i64, db: &Database) -> Result<Vec<Workspace>> {
		let mut workspaces: Vec<Workspace> = vec![];
		let mut statement = db
			.connection
			.prepare(
				"SELECT * FROM Workspace w
	INNER JOIN User_Workspace uw ON uw.id_workspace = w.id
	AND uw.id_user = :id_user",
			)?;

		statement.bind_iter(vec![(":id_user", Value::Integer(id_user.into()))])?;
		while let Ok(State::Row) = statement.next() {
			let workspace = Workspace {
				id: statement.read::<i64, _>("id").unwrap(),
				name: statement.read::<String, _>("name").unwrap(),
				collections: vec![],
			};
			workspaces.push(workspace);
		}

		Ok(workspaces)
	}

	/// Fetches all Collections from a Workspace id
	///
	/// * `id_workspace` - a i64 id to the corresponding workspace
	/// * `db` - Database to work on.
	///Returns a Vec of Collections
	pub fn get_all_collections(id_workspace: i64, db: &Database) -> Result<Vec<Collection>> {
		let mut collections: Vec<Collection> = vec![];

		let mut statement = db
			.connection
			.prepare(
				"SELECT * FROM Collection 
	WHERE id_workspace = :id_workspace",
			)?;

		statement.bind_iter(vec![(":id_workspace", Value::Integer(id_workspace.into()))])?;

		while let Ok(State::Row) = statement.next() {
			let collection = Collection {
				id: statement.read::<i64, _>("id").unwrap(),
				name: statement.read::<String, _>("name").unwrap(),
				queries: vec![],
			};
			collections.push(collection);
		}
		Ok(collections)
	}

	/// Fetches a request from it's id
	///
	/// * `id` - the i64 id of the request
	/// * `db` - Database to work on.
	pub fn get_request(id: i64, db: &Database) -> Result<Request> {
		let mut statement = db
			.connection
			.prepare(
				"SELECT * FROM Request 
	WHERE id = :id",
			)?;

		statement.bind_iter(vec![(":id", Value::Integer(id.into()))])?;

		if let Ok(State::Row) = statement.next() {
			let request = Request {
				id: statement.read::<i64, _>("id").unwrap(),
				name: statement.read::<String, _>("name").unwrap(),
				method: Methods::from_str(statement.read::<String, _>("method").as_ref().unwrap()).unwrap(),
				url: statement.read::<String, _>("url").unwrap(),
				params: statement.read::<String, _>("params").unwrap_or(String::from("")),
				body: statement.read::<String, _>("body").unwrap_or(String::from("")),
			};
			Ok(request)
		} else {
			Err(Error {
				code: Some(1),
				message: Some(String::from(
					"Something went wrong, User_Workspace entry not created.",
					)
				)},
			)
		}
	}

	/// Fetches all Requests from a Workspace id
	///
	/// * `id_collection` - a i64 id to the corresponding workspace
	/// * `db` - Database to work on.
	///Returns a Vec of Requests
	pub fn get_all_requests(id_collection: i64, db: &Database) -> Result<Vec<Request>> {
		let mut requests: Vec<Request> = vec![];

		let mut statement = db
			.connection
			.prepare(
				"SELECT * FROM Request 
	WHERE id_collection = :id_collection",
			)?;

		statement.bind_iter(vec![(
			":id_collection",
			Value::Integer(id_collection.into()),
		)])?;
		while let Ok(State::Row) = statement.next() {
			let request = Request {
				id: statement.read::<i64, _>("id").unwrap(),
				name: statement.read::<String, _>("name").unwrap(),
				method: Methods::from_str(statement.read::<String, _>("method").as_ref().unwrap()).unwrap(),
				url: statement.read::<String, _>("url").unwrap(),
				params: statement.read::<String, _>("params").unwrap_or(String::from("")),
				body: statement.read::<String, _>("body").unwrap_or(String::from("")),
		};
		requests.push(request);
	}
	Ok(requests)
}

pub fn create_environment(key: &str, db: &Database) -> Result<()> {
	let mut statement = db
		.connection
		.prepare("INSERT INTO Environmnent(name) VALUES (:name);")?;

	statement.bind_iter(vec![(":name", Value::String(key.to_owned()))])?;
	statement.next()?;
	Ok(())
}

pub fn create_variable_key(
	key: &str,
	id_environment: i64,
	id_workspace: i64,
	db: &Database,
) -> Result<()> {
	let mut statement = db.connection.prepare(
		"
INSERT INTO Variable_Key(key, id_workspace, id_environment) values (:key,
:id_workspace, :id_environment);
INSERT INTO Variable_Key(key, id_workspace, id_environment) values (:key,
:id_workspace, :id_environment);
",
	)?;

	statement.bind_iter(vec![
		(":key", Value::String(key.to_owned())),
		(":id_environment", Value::Integer(id_environment.into())),
		(":id_workspace", Value::Integer(id_workspace.into())),
	])?;
	statement.next()?;
	Ok(())
}

pub fn update_variable(id: i64, key: &str, value: &str, db: &Database) -> Result<()> {
	let mut statement = db.connection.prepare(
		"UPDATE Variable_Key
SET key = :key
WHERE id = :id;
",
	)?;

	statement.bind_iter::<_, (_, Value)>([
		(":key", key.into()),
		(":id", id.into()),
	])?;
	statement.next()?;
	statement.reset()?;
	let mut statement = db.connection.prepare(
		"UPDATE Variable_Value
SET value = :value
WHERE id_variable_key = :id;
",
	)?;
	statement.bind_iter::<_, (_, Value)>([
		(":value", value.into()),
		(":id", id.into()),
	])?;
	statement.next()?;
	Ok(())
}

pub fn get_all_variables(id_environment: i64, db: &Database) -> Result<Vec<Variable>> {
	let mut variables: Vec<Variable> = vec![];
	let mut statement = db
		.connection
		.prepare(
			"SELECT key, value FROM variables
WHERE id_environment = :id_environment;",
		)?;
	statement.bind_iter(vec![(
		":id_environment",
		Value::Integer(id_environment.into()),
	)])?;
	let mut idx = 1;
	while let Ok(State::Row) = statement.next() {
		let variable = Variable {
			id: idx,
			key: statement.read::<String, _>("key").unwrap(),
			value: statement.read::<String, _>("value").unwrap(),
		};
		variables.push(variable);
		idx +=1;
	}

	Ok(variables)
}
