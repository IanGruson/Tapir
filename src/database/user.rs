use crate::util::dbhandler::*;

use sqlite::*;
use std::*;

pub struct User {
	pub id: i64,
	pub name: String,
	pub email: String,
}

/// Gets a user from database using it's id.
/// param :
///
/// user_id - the user's id
///
/// Returns a User struct instance.
pub fn get_user(user_id: i64, db: &Database) -> Result<Option<User>> {
	let mut statement = db
		.connection
		.prepare("SELECT * FROM User WHERE id = :user_id;")
		.unwrap();
	statement.bind_iter(vec![(":user_id", Value::Integer(user_id.into()))])?;

	if let Ok(State::Row) = statement.next() {
		let user = User {
			id: statement.read::<i64, _>("id").unwrap(),
			name: statement.read::<String, _>("name").unwrap(),
			email: statement.read::<String, _>("email").unwrap(),
		};
		Ok(Some(user))
		//
		// This is freaking weird. Trying to make proper error handling but it's
		// a freaking mess.
	} else {
		Err(Error {
			code: Some(1),
			message: Some(String::from(
				"Something went wrong, User_Workspace entry not created.",
				)
			)},
		)
	}
}
