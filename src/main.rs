use std::{error::Error, io};

use termion::{event::Key, raw::IntoRawMode};
use tui::{backend::CrosstermBackend, Terminal};

use tapir::Application;
use tapir::{
	database::container,
	ui::{
		controls::{select, selection_mode_navigate, tab_select, Movement},
		view,
	},
	util::{
		dbhandler,
		event::{Event, Events},
	},
};

fn main() -> Result<(), Box<dyn Error>> {
	let stdout = io::stdout().into_raw_mode()?;
	// Next line renders interface in an alternate screen, meaning when exitting the application,
	// the interface compeletely vanishes from the terminal (same for stderr though)
	// let stdout = AlternateScreen::from(stdout);
	let backend = CrosstermBackend::new(stdout);
	let mut terminal = Terminal::new(backend)?;

	let mut app = Application::App::default();

	let events = Events::new();

	app.workspace_state.select(Some(0));
	app.col_state.select(Some(0));
	app.req_state.select(Some(0));
	app.variable_state.select(Some(0));

	let db = &dbhandler::Database {
		filename: String::from("./.database"),
		connection: sqlite::open("./.database")?,
	};

	let variables = container::get_all_variables(1, db)?;
	app.variables_map = view::variable_to_hash_map::<container::Variable>(variables);

	// This is here for tests mainly.
	let user = &tapir::database::user::get_user(1, db)?.unwrap();

	terminal.clear()?;

	loop {
		//terminal.autoresize or not it seems to resize automatically anyway.
		//So this is probably not needed.
		terminal.autoresize()?;

		terminal.draw(|f| {
			if app.selected_tab == 0 {
				tapir::ui::screen::home_screen::render_home_screen(f, &mut app, &db).unwrap();
			} else if app.selected_tab == 1 {
				tapir::ui::screen::environment_variables::render_env_var_screen(f, &mut app, db).unwrap();
			}
		})?;

		//call of the input event handler
		if let Event::Input(input) = events.next()? {
			match app.selected_tab {
				0 => {
					match app.input_mode {
						Application::InputMode::Normal => match input {
							Key::Char(' ') => match app.selection_mode {
								Application::SelectionMode::Collections => {
									app.selection_mode = Application::SelectionMode::Requests;
								}
								Application::SelectionMode::Requests => {
									app.selection_mode = Application::SelectionMode::Collections;
								}
								Application::SelectionMode::Response => {
									app.selection_mode = Application::SelectionMode::Collections;
								}
							},
							Key::Char('\n') => {
								if app.requests[app.req_state.selected().unwrap()].id as i64 == 0 {
									let response : Vec<String> = vec![String::from("You can't send a empty request! Make sure to add a request. Press 'c' to get help on how to add requests")];
									app.response = response;
								} else {
									let mut req_tmp = container::get_request(
										app.requests[app.req_state.selected().unwrap()].id as i64,
										db,
									)?;
									req_tmp.url =
										tapir::util::placeholder::replace_placeholder_in_string(
											&app.variables_map,
											&mut req_tmp.url,
										);
									let response: Vec<String> =
										match tapir::sender::url::send_request(req_tmp) {
											Ok(response) => response,
											Err(e) => vec![String::from(e.to_string())],
										};
									app.response = response;
								}
							}

							Key::Char('l') => {
								app.req_state.select(Some(0));
								app.col_state.select(Some(0));
								tab_select(
									&app.workspaces_line,
									&mut app.workspace_state,
									Movement::RIGHT,
								);
							}
							Key::Char('h') => {
								app.req_state.select(Some(0));
								app.col_state.select(Some(0));
								tab_select(
									&app.workspaces_line,
									&mut app.workspace_state,
									Movement::LEFT,
								);
							}

							Key::Char('k') => {
								let amount: usize = 1;
								selection_mode_navigate(&mut app, Movement::UP, amount);
							}
							Key::Char('j') => {
								let amount: usize = 1;
								selection_mode_navigate(&mut app, Movement::DOWN, amount);
							}
							Key::Char('G') => match app.selection_mode {
								Application::SelectionMode::Requests => {
									let amount: usize = ((app.request_list.len()) as i8
										- app.req_state.selected().unwrap() as i8)
										as usize - 1;
									select(
										&app.request_list,
										&mut app.req_state,
										Movement::DOWN,
										amount,
									);
								}
								Application::SelectionMode::Collections => {
									let amount: usize = ((app.collection_list.len()) as i8
										- app.col_state.selected().unwrap() as i8)
										as usize - 1;
									app.req_state.select(Some(0));
									select(
										&app.collection_list,
										&mut app.col_state,
										Movement::DOWN,
										amount,
									);
								}
								Application::SelectionMode::Response => {}
							},

							// Request and collections deletion.
							Key::Backspace => match app.selection_mode {
								Application::SelectionMode::Collections => {
									container::delete_collection(
										app.collections[app.col_state.selected().unwrap()].id
											as i64,
										db,
									)?;
								}
								Application::SelectionMode::Requests => {
									container::delete_request(
										app.requests[app.req_state.selected().unwrap()].id as i64,
										db,
									)?;
								}
								Application::SelectionMode::Response => {}
							},

							Key::Char('r') => {
								app.selection_mode = Application::SelectionMode::Response;
							}

							_ => {}
						},
						Application::InputMode::Editing => {}
						Application::InputMode::HelpControls => match input {
							Key::Char('q') => {
								app.input_mode = Application::InputMode::Normal;
							}
							Key::Char('?') => {
								app.input_mode = Application::InputMode::Normal;
							}
							Key::Esc => {
								app.input_mode = Application::InputMode::Normal;
							}
							_ => {}
						},
						Application::InputMode::HelpCommands => match input {
							Key::Char('q') => {
								app.input_mode = Application::InputMode::Normal;
							}
							Key::Esc => {
								app.input_mode = Application::InputMode::Normal;
							}
							_ => {}
						},
						_ => {}
					}
				}

				1 => {
					app.event_input = input;
					match app.input_mode {
						Application::InputMode::Normal => {
							match input {
								Key::Char('k') => {
									let amount: usize = 1;
									select(
										&app.variables_list,
										&mut app.variable_state,
										Movement::UP,
										amount,
									);
								}
								Key::Char('j') => {
									let amount: usize = 1;
									select(
										&app.variables_list,
										&mut app.variable_state,
										Movement::DOWN,
										amount,
									);
								}
								Key::Char('\n') => {
									if !app.variables_list.is_empty() {
										let var_idx = app.variable_state.selected().unwrap();
										let tmp_var = app.variables_list[var_idx as usize].clone();
										// app.variables_list.remove(var_idx);
										app.input_mode = Application::InputMode::Editing;
									}
								}
								_ => {}
							}
						}
						Application::InputMode::Editing => match input {
							Key::Char(c) => {
								if c.ne(&'\n') {
									app.input.push(c);
								}
							}
							_ => {}
						},
						Application::InputMode::HelpCommands => match input {
							Key::Char('q') => {
								app.input_mode = Application::InputMode::Normal;
							}
							Key::Esc => {
								app.input_mode = Application::InputMode::Normal;
							}
							_ => {}
						},

						_ => {}
					}
				}
				_ => {}
			}
			match app.input_mode {
				Application::InputMode::Normal => {
					match input {
						// ---- Workspaces -----
						Key::Char('1') => {
							// Go to workspace 1
							app.selected_tab = 0;
						}
						Key::Char('2') => {
							// Go to workspace 2
							app.selected_tab = 1;
						}
						Key::Char('3') => {
							// Go to workspace 2
							app.selected_tab = 2;
						}
						Key::Char('4') => {
							// Go to workspace 2
							app.selected_tab = 3;
						}
						Key::Char('q') => {
							break;
						}
						Key::Char(':') => {
							app.input_mode = Application::InputMode::Command;
						}

						Key::Char('?') => {
							app.input_mode = Application::InputMode::HelpControls;
						}

						Key::Char('c') => {
							app.input_mode = Application::InputMode::HelpCommands;
						}
						_ => {}
					}
				}
				Application::InputMode::Command => match input {
					Key::Char('\n') => match app.selected_tab {
						0 => {
							tapir::util::command_line::command_line(&mut app, &db, &user)?;
						}
						1 => {
							tapir::util::command_line::env_variable_commands(&mut app, &db, &user)?;
						}
						_ => {}
					},
					Key::Char(c) => {
						app.input_command.push(c);
					}
					Key::Backspace => {
						app.input_command.pop();
					}
					Key::Esc => {
						app.input_command.drain(..);
						app.input_mode = Application::InputMode::Normal;
					}
					_ => {}
				},
				_ => {}
			}
		};
	}
	Ok(())
}
