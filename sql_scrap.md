CREATE TABLE Variable_Key(
id INTEGER PRIMARY KEY NOT NULL,
key TEXT NOT NULL,
id_workspace INTEGER NOT NULL,
id_environment INTEGER NOT NULL,
FOREIGN KEY(id_workspace) REFERENCES Workspace(id) ON DELETE CASCADE,
FOREIGN KEY(id_environment) REFERENCES Environment(id) ON DELETE CASCADE);

CREATE TABLE Variable_Value(
id INTEGER PRIMARY KEY NOT NULL,
value TEXT NOT NULL,
id_environment INTEGER NOT NULL,
id_variable_key INTEGER NOT NULL,
FOREIGN KEY(id_variable_key) REFERENCES Variable_Key(id) ON DELETE CASCADE,
FOREIGN KEY(id_environment) REFERENCES Environment(id) ON DELETE CASCADE);

CREATE TABLE Environment_Variable_Key(
id_environment INTEGER NOT NULL,
id_variable_key INTEGER NOT NULL,
FOREIGN KEY(id_environment) REFERENCES Environment(id) ON DELETE CASCADE,
FOREIGN KEY(id_variable_key) REFERENCES Variable_Key(id) ON DELETE CASCADE);

INSERT INTO Variable_Key(key, id_workspace, id_environment) values ('test_key',
1, 1);
INSERT INTO Environment_Variable_Key(id_environment, id_variable_key) values (1,
1);
INSERT INTO Variable_Value(value, id_variable_key, id_environment) values
('test_value', 1, 1);

SELECT key, vv.value AS value FROM Variable_Key INNER JOIN Variable_Value AS vv
ON vv.id = Variable_Key.id;

SELECT key,
vv.value AS value,
env_k.id_environment as id_environment
FROM Variable_Key INNER JOIN Variable_Value AS vv
ON vv.id = Variable_Key.id
INNER JOIN Environment_Variable_Key as env_k
ON env_k.id_variable_key = Variable_Key.id
WHERE env_k.id_environment = 1;
